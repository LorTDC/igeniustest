//
//  GridPhotoCell.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 23/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import UIKit
import Kingfisher

class GridPhotoCell: UICollectionViewCell {

    struct ViewModel {
        let imageUrl: URL
        
        init(_ media: IGMedia) {
            imageUrl = media.images.lowResolution.url
        }
    }
    
    @IBOutlet var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
    }
    
    func setup(with model: ViewModel) {
        imageView.kf.setImage(with: model.imageUrl)
    }
}
