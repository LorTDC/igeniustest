//
//  InstagramAPI.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 19/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import Foundation
import Alamofire

struct InstagramAPI {
    
    private static let authUrl = "https://api.instagram.com/oauth/authorize/"
    private static let clientId = "ac84c244845c43d08b5ea6d928a758b0"
    private static let clientSecret = "1df00fc0d98043d1aa2fb39caf9388e5"
    private static let redirectURI = "https://github.com/LorTos"
    
    static var authURL: URL? {
        let queryString = "?client_id=\(InstagramAPI.clientId)&redirect_uri=\(InstagramAPI.redirectURI)&response_type=token&scope=basic&DEBUG=True"
        return URL(string: InstagramAPI.authUrl + queryString)
    }
    
    static func accessToken(fromRequest request: URLRequest) -> String? {
        guard   let requestURLString = request.url?.absoluteString,
                requestURLString.hasPrefix(InstagramAPI.redirectURI),
                let range = requestURLString.range(of: "#access_token=")  else
        {
            return nil
        }
        
        let accessToken = String(requestURLString[range.upperBound...])
        return accessToken
    }
    
    static func getRecentMedia(forUserWithAccessToken accessToken: String, completionHandler: @escaping (Result<IGResponse<[IGMedia]>, Error>) -> Void) {
        let getMediaUrl = "https://api.instagram.com/v1/users/self/media/recent?access_token=\(accessToken)"
        let params: Parameters = ["COUNT": 30]
        AF.request(getMediaUrl, method: .get, parameters: params).response { response in
            guard let data = response.data else {
                let error = response.error ?? AFError.invalidURL(url: getMediaUrl)
                completionHandler(.failure(error))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let media = try decoder.decode(IGResponse<[IGMedia]>.self, from: data)
                completionHandler(.success(media))
            } catch {
                completionHandler(.failure(error))
            }
        }
    }
}
