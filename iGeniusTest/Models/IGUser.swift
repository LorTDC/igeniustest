//
//  IGUser.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 22/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import Foundation

struct IGUser: Decodable {
    
    let id: String
    let username: String
    let profilePicture: URL
    let fullName: String
    let bio: String?
    let counts: Counts?
    
    struct Counts: Decodable {
        let media: Int
        let follows: Int
        let followedBy: Int
    }
}
