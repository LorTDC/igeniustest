//
//  IGMedia.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 22/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//
import CoreLocation

class IGMedia: Decodable {
    
    let id: String
    let user: IGUser
    let createdDate: Date
    let type: String
    let link: URL
    let location: IGLocation?
    
    let images: Images
    let videos: Videos?
    let carouselMedia: [CarouselMedia]?
    let caption: IGComment?
    let likes: Count
    
    struct Resolution: Decodable {
        let width: Int
        let height: Int
        let url: URL
    }
    
    struct Images: Decodable {
        let thumbnail: Resolution
        let lowResolution: Resolution
        let standardResolution: Resolution
    }
    struct Videos: Decodable {
        let lowBandwidth: Resolution
        let lowResolution: Resolution
        let standardResolution: Resolution
    }
    struct CarouselMedia: Decodable {
        let images: Images?
        let videos: Videos?
        let type: String
    }
    struct Count: Decodable {
        let count: Int
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, user, createdTime, type, images, videos, caption, likes, link, location, carouselMedia
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        user = try container.decode(IGUser.self, forKey: .user)
        let createdTime = try container.decode(String.self, forKey: .createdTime)
        createdDate = Date(timeIntervalSince1970: Double(createdTime)!)
        type = try container.decode(String.self, forKey: .type)
        link = try container.decode(URL.self, forKey: .link)
        location = try container.decodeIfPresent(IGLocation.self, forKey: .location)
        images = try container.decode(Images.self, forKey: .images)
        videos = try container.decodeIfPresent(Videos.self, forKey: .videos)
        carouselMedia = try container.decodeIfPresent([CarouselMedia].self, forKey: .carouselMedia)
        caption = try container.decodeIfPresent(IGComment.self, forKey: .caption)
        likes = try container.decode(Count.self, forKey: .likes)
    }
}
