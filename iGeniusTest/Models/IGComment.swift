//
//  IGComment.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 22/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import Foundation

struct IGComment: Decodable {
    
    let id: String
    let text: String
    let from: IGUser
    let createdDate: Date
    
    private enum CodingKeys: String, CodingKey {
        case id, text, from, createdTime
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        text = try container.decode(String.self, forKey: .text)
        from = try container.decode(IGUser.self, forKey: .from)
        let createdTime = try container.decode(String.self, forKey: .createdTime)
        createdDate = Date(timeIntervalSince1970: Double(createdTime)!)
    }
}
