//
//  IGLocation.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 22/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import CoreLocation

struct IGLocation: Decodable {
    
    let id: Int
    let name: String
    let streetAddress: String?
    let coordinates: CLLocationCoordinate2D
    
    private enum CodingKeys: String, CodingKey {
        case id, name, streetAddress, latitude, longitude
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        streetAddress = try container.decodeIfPresent(String.self, forKey: .streetAddress)
        let latitude = try container.decode(Double.self, forKey: .latitude)
        let longitude = try container.decode(Double.self, forKey: .longitude)
        coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
