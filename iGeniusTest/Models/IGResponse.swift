//
//  IGResponse.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 22/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import Foundation

struct IGResponse<T: Decodable>: Decodable {

    let data: T?
    let meta: Meta
    let pagination: Pagination?

    struct Meta: Decodable {
        let code: Int
        let errorType: String?
        let errorMessage: String?
    }

    struct Pagination: Decodable {
        let nextUrl: String?
        let nextMaxId: String?
    }
}
