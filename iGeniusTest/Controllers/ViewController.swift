//
//  ViewController.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 17/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import UIKit
import SafariServices

class ViewController: UIViewController {

    @IBOutlet var buttonContainerView: UIView!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var actvityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBackgroundGradientLayer()
        setupLoginButton()
    }
    
    private func setupBackgroundGradientLayer() {
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [
            UIColor.igBlue.cgColor,
            UIColor.igPurple.cgColor,
            UIColor.igRed.cgColor,
            UIColor.igOrange.cgColor,
            UIColor.igYellow.cgColor,
        ]
        gradient.startPoint = CGPoint(x: 0.8, y: 0)
        gradient.endPoint = CGPoint(x: 0.2, y: 1)
        view.layer.insertSublayer(gradient, below: buttonContainerView.layer)
    }
    
    private func setupLoginButton() {
        actvityIndicator.hidesWhenStopped = true
        actvityIndicator.stopAnimating()
        
        loginButton.layer.cornerRadius = 12
        loginButton.layer.masksToBounds = true
        
        buttonContainerView.backgroundColor = .clear
        buttonContainerView.layer.shadowPath = UIBezierPath(roundedRect: loginButton.bounds, cornerRadius: loginButton.layer.cornerRadius).cgPath
        buttonContainerView.layer.shouldRasterize  = true
        buttonContainerView.layer.rasterizationScale = UIScreen.main.scale
        buttonContainerView.layer.shadowColor = UIColor.black.cgColor
        buttonContainerView.layer.shadowOpacity = 0.2
        buttonContainerView.layer.shadowRadius = 10
        buttonContainerView.layer.shadowOffset = CGSize(width: 0, height: 4)
    }
    
    @IBAction func tappedOnLogin(_ sender: UIButton) {
        let authController = AuthenticationWebViewController()
        authController.delegate = self
        let navController = UINavigationController(rootViewController: authController)
        present(navController, animated: true, completion: nil)
    }
    private func setButtonEnabled(_ enabled: Bool) {
        loginButton.isEnabled = enabled
        UIView.animate(withDuration: 0.42) {
            self.loginButton.alpha = enabled ? 1 : 0.65
        }
    }
}

extension ViewController: AuthenticationWebViewControllerDelegate {
    func didReceiveAccessToken(_ accessToken: String) {
        // Dismiss web auth controller
        presentedViewController?.dismiss(animated: true, completion: nil)
        // Save access token
        UserDefaults.standard.set(accessToken, forKey: "access_token")
        // Present PhotoGrid
        let photoGridController = PhotoGridViewController()
        let navController = UINavigationController(rootViewController: photoGridController)
        navController.modalPresentationStyle = .fullScreen
        present(navController, animated: true, completion: nil)
    }
}
