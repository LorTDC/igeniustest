//
//  PhotoGridViewController.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 22/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import UIKit
import IGListKit

class PhotoGridViewController: UIViewController {
    
    @IBOutlet var collectionView: ListCollectionView!
    
    private lazy var collectionLayout: ListCollectionViewLayout = {
        return ListCollectionViewLayout(stickyHeaders: true, scrollDirection: .vertical, topContentInset: 1, stretchToEdge: false)
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .gray)
        indicator.hidesWhenStopped = true
        indicator.stopAnimating()
        return indicator
    }()
    
    private lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    private var dataSource: [IGMedia] = [] {
        didSet {
            adapter.performUpdates(animated: true, completion: nil)
        }
    }
    
    private var accessToken: String? {
        return UserDefaults.standard.string(forKey: "access_token")
    }
    
    init() {
        super.init(nibName: "PhotoGridViewController", bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) not implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigation()
        setupCollection()
        fetchData()
    }
    
    private func setupNavigation() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logout))
    }
    
    private func setupCollection() {
        collectionView.setCollectionViewLayout(collectionLayout, animated: false)
        collectionView.backgroundView = activityIndicator
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
    
    private func fetchData() {
        guard let accessToken = accessToken else { return }
        
        activityIndicator.startAnimating()
        InstagramAPI.getRecentMedia(forUserWithAccessToken: accessToken) { response in
            self.activityIndicator.stopAnimating()
            switch response {
            case .success(let result):
                self.dataSource += result.data ?? []
            case .failure(let error):
                let alert = UIAlertController.errorAlert(error) { [weak self] in
                    self?.fetchData()
                }
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc private func logout() {
        UserDefaults.standard.removeObject(forKey: "access_token")
        dismiss(animated: true, completion: nil)
    }
}

extension PhotoGridViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return dataSource
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return PhotoGridSectionController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}
