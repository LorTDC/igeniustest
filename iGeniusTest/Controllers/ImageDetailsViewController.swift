//
//  ImageDetailsViewController.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 23/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import UIKit
import Kingfisher

class ImageDetailsViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    
    private let media: IGMedia
    
    init(media: IGMedia) {
        self.media = media
        super.init(nibName: "ImageDetailsViewController", bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.kf.setImage(with: media.images.standardResolution.url)
    }
}
