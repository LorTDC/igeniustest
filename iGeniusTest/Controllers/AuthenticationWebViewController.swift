//
//  AuthenticationWebViewController.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 19/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import UIKit
import WebKit

protocol AuthenticationWebViewControllerDelegate: class {
    func didReceiveAccessToken(_ accessToken: String)
}

class AuthenticationWebViewController: UIViewController {
    
    private var webView = UIWebView(frame: .zero)
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(style: .gray)
        let size: CGFloat = 50
        loader.frame = CGRect(origin: CGPoint(x: view.frame.midX - (size / 2) , y: view.frame.midY - (size / 2)),
                              size: CGSize(width: size, height: size))
        loader.hidesWhenStopped = true
        return loader
    }()
    
    weak var delegate: AuthenticationWebViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
        setupNav()
        
        guard let url = InstagramAPI.authURL else { return }
        let myRequest = URLRequest(url: url)
        webView.loadRequest(myRequest)
    }
    
    private func setupWebView() {
        webView.delegate = self
        webView.frame = view.bounds
        view.addSubview(webView)
        var topAnchor = view.layoutMarginsGuide.topAnchor
        if #available(iOS 11.0, *) { topAnchor = view.safeAreaLayoutGuide.topAnchor }
        webView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    private func setupNav() {
        navigationItem.titleView = activityIndicator
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissController))
    }
    
    @objc private func dismissController() {
        dismiss(animated: true, completion: nil)
    }
}

extension AuthenticationWebViewController: UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        if webView.isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
        return checkRequestForCallbackURL(request: request)
    }
    private func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        if let accessToken = InstagramAPI.accessToken(fromRequest: request) {
            delegate?.didReceiveAccessToken(accessToken)
            return false
        }
        return true
    }
}
