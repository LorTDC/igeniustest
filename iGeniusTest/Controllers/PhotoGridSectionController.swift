//
//  PhotoGridSectionController.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 23/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import UIKit
import IGListKit

class PhotoGridSectionController: ListSectionController {
    
    private var media: IGMedia?
    
    override func didUpdate(to object: Any) {
        if let mediaObject = object as? IGMedia {
            media = mediaObject
        }
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let context = collectionContext, let media = media else {
            return UICollectionViewCell()
        }
        let cell = context.dequeueReusableCell(withNibName: "GridPhotoCell", bundle: nil, for: self, at: index)
        guard let gridCell = cell as? GridPhotoCell else {
            return cell
        }
        let viewModel = GridPhotoCell.ViewModel(media)
        gridCell.setup(with: viewModel)
        return gridCell
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        let dimension = width / 3
        return CGSize(width: dimension , height: dimension)
    }
    
    override func didSelectItem(at index: Int) {
        guard let gridController = viewController as? PhotoGridViewController, let media = media else { return }
        
        let imageDetailsController = ImageDetailsViewController(media: media)
        gridController.navigationController?.pushViewController(imageDetailsController, animated: true)
    }
}
