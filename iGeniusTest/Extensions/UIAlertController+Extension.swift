//
//  UIAlertController+Extension.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 23/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func errorAlert(_ error: Error, retryActionHandler handler: (() -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        if let retryHandler = handler {
            let retryAction = UIAlertAction(title: "Retry", style: .default) { _ in retryHandler() }
            alert.addAction(retryAction)
        }
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        return alert
    }
}
