//
//  IGMedia+Extension.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 23/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import Foundation
import IGListKit

extension IGMedia: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSString
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object as? IGMedia else { return false }
        return object.id == self.id
    }
}
