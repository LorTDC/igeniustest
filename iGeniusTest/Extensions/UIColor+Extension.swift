//
//  UIColor+Extension.swift
//  iGeniusTest
//
//  Created by Lorenzo Toscani De Col on 17/09/2019.
//  Copyright © 2019 Lorenzo Toscani De Col. All rights reserved.
//

import UIKit

extension UIColor {
    static var igBlue: UIColor {
        return UIColor(red: 79/255, green: 91/255, blue: 213/255, alpha: 1)
    }
    static var igPurple: UIColor {
        return UIColor(red: 150/255, green: 47/255, blue: 191/255, alpha: 1)
    }
    static var igRed: UIColor {
        return UIColor(red: 214/255, green: 41/255, blue: 118/255, alpha: 1)
    }
    static var igOrange: UIColor {
        return UIColor(red: 250/255, green: 126/255, blue: 30/255, alpha: 1)
    }
    static var igYellow: UIColor {
        return UIColor(red: 254/255, green: 218/255, blue: 117/255, alpha: 1)
    }
}
